package com.spreedly.async.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.api.client.http.GenericUrl;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class ProxyFactory {

    @JsonProperty
    @NotEmpty
    private String submissionUrl;

    @JsonProperty
    @NotEmpty
    private String callbackUrl;

    @JsonProperty
    @NotNull
    private Long timeout;

    public String getSubmissionUrl() {
        return submissionUrl;
    }

    public void setSubmissionUrl(String submissionUrl) {
        this.submissionUrl = submissionUrl;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public Long getTimeout() {
        return timeout;
    }

    public void setTimeout(Long timeout) {
        this.timeout = timeout;
    }

    public GenericUrl buildSubmissionUrl() {
        return new GenericUrl(submissionUrl);
    }
}
