package com.spreedly.async;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import com.spreedly.async.repository.InMemoryJobRepository;
import com.spreedly.async.repository.JobRepository;
import com.spreedly.async.resources.ProxyResource;

public class AsynchronizrApplication extends Application<AsynchronizrConfiguration> {

    public static void main(final String[] args) throws Exception {
        new AsynchronizrApplication().run(args);
    }

    @Override
    public String getName() {
        return "Asynchronizr";
    }

    @Override
    public void initialize(final Bootstrap<AsynchronizrConfiguration> bootstrap) {
    }

    @Override
    public void run(final AsynchronizrConfiguration configuration,
                    final Environment environment) {

        JobRepository repository = new InMemoryJobRepository(configuration.getJobSize());

        ProxyResource resource = new ProxyResource(configuration.getProxyFactory(),
                repository);

        environment.jersey().register(resource);
    }

}
