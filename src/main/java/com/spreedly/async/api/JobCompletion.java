package com.spreedly.async.api;

import com.google.api.client.util.Key;
import com.google.common.base.MoreObjects;
import org.hibernate.validator.constraints.NotBlank;

public class JobCompletion {

    @NotBlank
    @Key
    private String state;

    @NotBlank
    @Key
    private String id;

    @NotBlank
    @Key
    private String startedAt;

    @NotBlank
    @Key
    private String proof;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    public String getProof() {
        return proof;
    }

    public void setProof(String proof) {
        this.proof = proof;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("state", state)
                .add("id", id)
                .add("startedAt", startedAt)
                .add("proof", proof)
                .toString();
    }
}
