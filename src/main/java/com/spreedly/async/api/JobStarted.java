package com.spreedly.async.api;

import com.google.api.client.util.Key;
import com.google.common.base.MoreObjects;
import org.hibernate.validator.constraints.NotBlank;

public class JobStarted {

    @NotBlank
    @Key
    private String state;

    @NotBlank
    @Key
    private String id;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("state", state)
                .add("id", id)
                .toString();
    }
}
