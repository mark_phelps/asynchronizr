package com.spreedly.async.api;

import com.google.api.client.util.Key;
import com.google.common.base.MoreObjects;
import org.hibernate.validator.constraints.NotBlank;

public class JobCreateRequest {

    @NotBlank
    @Key
    private String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("account", account)
                .toString();
    }
}
