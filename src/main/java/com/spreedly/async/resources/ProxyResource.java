package com.spreedly.async.resources;

import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.spreedly.async.api.JobCompletion;
import com.spreedly.async.api.JobCreateRequest;
import com.spreedly.async.api.JobStarted;
import com.spreedly.async.config.ProxyFactory;
import com.spreedly.async.repository.JobRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;

@Path("/proxy")
@Produces("application/json")
@Consumes("application/json")
public class ProxyResource {

    private static final Logger log = LoggerFactory.getLogger(ProxyResource.class);

    private static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private static final HttpRequestFactory REQUEST_FACTORY = new NetHttpTransport().createRequestFactory(
            new HttpRequestInitializer() {
                @Override
                public void initialize(HttpRequest httpRequest) throws IOException {
                    httpRequest.setParser(new JsonObjectParser(JSON_FACTORY));
                }
            });

    private final GenericUrl submissionUrl;
    private final String callbackUrl;
    private final Long timeout;

    private final JobRepository repository;

    public ProxyResource(ProxyFactory proxyFactory, JobRepository repository) {
        checkNotNull(proxyFactory);

        this.submissionUrl = proxyFactory.buildSubmissionUrl();
        this.callbackUrl = proxyFactory.getCallbackUrl();
        this.timeout = proxyFactory.getTimeout();

        this.repository = checkNotNull(repository);
    }

    // http://localhost:8080/proxy
    @POST
    public Response proxy(@Valid JobCreateRequest jobCreateRequest) {

        log.debug("Proxy jobCreateRequest={} received on thread={}", jobCreateRequest, Thread.currentThread().getName());

        Map<String, Object> json = new HashMap<>();
        json.put("account", jobCreateRequest.getAccount());
        json.put("wait", false);
        json.put("callback", callbackUrl);

        try {
            log.debug("Posting to url={}; account={}; callback={}", submissionUrl.toString(),
                    jobCreateRequest.getAccount(), callbackUrl);

            HttpRequest request = REQUEST_FACTORY.buildPostRequest(submissionUrl, new JsonHttpContent(JSON_FACTORY, json));

            Future<HttpResponse> future = request.executeAsync();
            HttpResponse response = future.get(timeout, TimeUnit.SECONDS);

            JobStarted jobStarted = response.parseAs(JobStarted.class);

            String jobId = jobStarted.getId();

            log.debug("Response: status={}; body={}", response.getStatusCode(), jobStarted);

            while (!repository.isCompleted(jobId)) {
                Thread.sleep(1L);
            }

            JobCompletion jobCompletion = repository.get(jobId);

            return Response.ok(jobCompletion).build();

        } catch (Exception e) {
            log.error("Exception: ", e);
            return Response.serverError().build();
        }
    }

    // http://localhost:8080/proxy/callback
    @Path("/callback")
    @POST
    public Response callback(@Valid JobCompletion jobCompletion) {
        log.debug("Callback request received on thread={}", Thread.currentThread().getName());

        repository.complete(jobCompletion);

        log.debug("Job completed={}", jobCompletion);

        return Response.ok().build();
    }
}