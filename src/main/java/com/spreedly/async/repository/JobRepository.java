package com.spreedly.async.repository;

import com.spreedly.async.api.JobCompletion;

public interface JobRepository {

    boolean isCompleted(String jobId);

    JobCompletion get(String jobId);

    void complete(JobCompletion jobCompletion);
}
