package com.spreedly.async.repository;

import com.google.common.annotations.VisibleForTesting;
import com.spreedly.async.api.JobCompletion;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryJobRepository implements JobRepository {

    private final Map<String, JobCompletion> completions;

    public InMemoryJobRepository(int jobSize) {
        completions = new ConcurrentHashMap<>(jobSize);
    }

    @Override
    public boolean isCompleted(String jobId) {
        return completions.containsKey(jobId);
    }

    @Override
    public JobCompletion get(String jobId) {
        return completions.get(jobId);
    }

    @Override
    public void complete(JobCompletion jobCompletion) {
        completions.putIfAbsent(jobCompletion.getId(), jobCompletion);
    }

    @VisibleForTesting
    void clear() {
        completions.clear();
    }
}
