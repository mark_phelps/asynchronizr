package com.spreedly.async;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.spreedly.async.config.ProxyFactory;
import io.dropwizard.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class AsynchronizrConfiguration extends Configuration {

    @Valid
    @JsonProperty("proxy")
    private ProxyFactory proxyFactory;

    @NotNull
    @JsonProperty("defaultJobSize")
    private Integer jobSize;

    public ProxyFactory getProxyFactory() {
        return proxyFactory;
    }

    public void setProxyFactory(ProxyFactory proxyFactory) {
        this.proxyFactory = proxyFactory;
    }

    public Integer getJobSize() {
        return jobSize;
    }

    public void setJobSize(Integer jobSize) {
        this.jobSize = jobSize;
    }
}
