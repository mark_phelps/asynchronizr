package com.spreedly.async.repository;

import com.spreedly.async.api.JobCompletion;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class InMemoryJobRepositoryTest {

    private final InMemoryJobRepository repository = new InMemoryJobRepository(2);

    @Before
    public void setUp() {
        repository.clear();
    }

    @Test
    public void testIsCompleted() {
        assertFalse(repository.isCompleted("test"));

        JobCompletion completion = new JobCompletion();
        completion.setId("test");

        repository.complete(completion);

        assertTrue(repository.isCompleted("test"));
    }

    @Test
    public void testGet() {
        assertNull(repository.get("test"));

        JobCompletion completion = new JobCompletion();
        completion.setId("test");

        repository.complete(completion);

        assertNotNull(repository.get("test"));
        assertEquals("test", repository.get("test").getId());
    }

    @Test
    public void testComplete() {
        JobCompletion completion = new JobCompletion();
        completion.setId("test");

        repository.complete(completion);

        assertTrue(repository.isCompleted("test"));
    }
}
