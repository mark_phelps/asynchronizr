# Asynchronizr

A [Dropwizard](http://www.dropwizard.io/0.9.2/docs/) implementation of an asynchronous proxy for the [Spreedly Job Server](https://gist.github.com/rwdaigle/67592c27394fe8a5c4ac83fdeaaa42d8).

## Prerequisites

* Java 8
* Maven 3
* Ngrok (if not publicly accessible)

## Config

1. Update app.yml and set `callbackUrl`

## Running

1. `$ mvn clean -U package`
1. `$ java -jar target/asynchronizr-1.0-SNAPSHOT.jar server app.yml`
1. `$ jobclient --url http://localhost:8080/proxy --account CHANGE_ME`

## Example

```
$ ~/Downloads/jobclient --url http://localhost:8080/proxy --account SCRUBBED
50 / 50 [===========================================================================================================================================================] 100.00 % 1s

Succeeded: 50
Average Duration: 0.763194417 seconds
Total Duration: 1.016776822 seconds

WORK SAMPLE RESULT: PASSED

Proof (supply to Spreedly):
H4sIAAAJbogCHVmIKkJlXSUeOEuIX46 .... jL//HwAA//9iuCqnsxEAAA==
```

### Job Requests

```
DEBUG [2016-04-14 23:31:34,002] com.spreedly.async.resources.ProxyResource: Proxy jobCreateRequest=JobCreateRequest{account=SCRUBBED} received on thread=dw-13
7 - POST /proxy
DEBUG [2016-04-14 23:31:34,002] com.spreedly.async.resources.ProxyResource: Posting to url=http://jobs.asgateway.com/start; account=SCRUBBED; callback=http://
5c4cf3db.ngrok.io/proxy/callback
```

### Jobs Started

```
DEBUG [2016-04-14 23:31:34,117] com.spreedly.async.resources.ProxyResource: Response: status=200; body=JobStarted{state=running, id=5023ae22d0393dc8}
DEBUG [2016-04-14 23:31:34,117] com.spreedly.async.resources.ProxyResource: Response: status=200; body=JobStarted{state=running, id=cd341f5461aa789}
DEBUG [2016-04-14 23:31:34,132] com.spreedly.async.resources.ProxyResource: Response: status=200; body=JobStarted{state=running, id=6bd93ee9dafcdafe}
DEBUG [2016-04-14 23:31:34,141] com.spreedly.async.resources.ProxyResource: Response: status=200; body=JobStarted{state=running, id=477f4b476f9e6c50}
```
### Job Completion
```
DEBUG [2016-04-14 23:31:35,023] com.spreedly.async.resources.ProxyResource: Callback request received on thread=dw-136 - POST /proxy/callback
DEBUG [2016-04-14 23:31:35,023] com.spreedly.async.resources.ProxyResource: Job completed=JobCompletion{state=completed, id=19e6235a35d49ea2, startedAt=2016-04-14T23:31:35.085Z, proof=1DC2DA1A766B0C0F28EE0CE16C4C85B051E7F229}
DEBUG [2016-04-14 23:31:35,023] com.spreedly.async.resources.ProxyResource: Job completed=JobCompletion{state=completed, id=b34c55dca9ad1b79, startedAt=2016-04-14T23:31:35.085Z, proof=51A6A77179292D966805DF82CC7EFD0993DB8102}
```

## Questions/Answers

### 1. What error conditions is the service not designed to handle?

#### Timeouts/Connection Errors

In brief, the service is not designed to handle timeouts/connection errors gracefully. Below, I will cover each scenario that this could happen

1. **Submitting the Job Request to Spreedly** - I do allow for a timeout to be specified when trying to POST the `JobCreateRequest` to Spreedly, however if this request does in fact timeout (slow or no response from Spreedly), the service raises an exception and a 500 Server Error is returned. If this situation does occur, our service will continue to try and send POSTs to Spreedly each time, further compounding the issue. A circuit breaker pattern could be implemented here to prevent attempted connections to Spreedly once it has been determined to be unavailable.

2. **Receiving the Callback POST from Spreedly** - If Spreedly cannot successfully send the callback POST containing the `JobCompletion` JSON to our service (because of load balancer or DNS issues, or other internal errors), then the job will never be marked as complete, and the `jobclient` will timeout. Basically the threads that are waiting to return to the client will be deadlocked waiting on a successful `JobCompletion` notification.


#### Invalid Payloads/Other Responses

The service has not been designed to handle invalid or different request/response payloads from either the `jobclient` or the Spreedly servers. If any of the requests/responses differ from those defined in the problem, then the service will not be able to handle these gracefully.

Also the service expects that the request to Spreedly will always result in a 200 response status. If another status code is sent, the service may not function properly.

#### Duplicate Data

The service was designed with the assumption that no `JobCompletion` payloads will be sent twice with the same job ID.

#### Rate Limits

If Spreedly decided to Rate Limit our proxy service, then the service will not be able to handle this appropriately. This ties in with the above mention of Invalid Responses. Also, the `JobCreateRequest` requests will be 'dropped on the floor' and never retried.

### 2. What are your recommendations for handling these error conditions in future development iterations?

#### Timeouts/Connection Errors

I would recommend setting timeouts on all external HTTP requests/responses made from the service. This includes both trying to submit the Job Create Request to Spreedly, as well as responding to the `jobclient` in time. If the request to Spreedly does not respond within the timeout then a `408 Timeout` should be sent back to the client. Similarly, if Spreedly does not hit our Callback endpoint in a certain amount of time with the Job Completion Notification, then our server should issue a `408 Timeout` back to the job client.

As mentioned above, a [Circuit Breaker pattern](http://martinfowler.com/bliki/CircuitBreaker.html) could be implemented so that requests fail fast if Spreedly is unavailable or taking too long to respond.

#### Invalid Payloads/Other Responses

The exact payload body and possible HTTP status codes and potential errors that can be received from the Spreedly Job API should be documented and understood. Additionally, Spreedly should be able to version their API so that our proxy can (somewhat) depend on this versioned specific combination of endpoints, payloads and HTTP status codes. 

Also, the proxy should be written in a way that is resilient to these potential differences whenever possible. For example, currently the JSON payloads contain all of the given fields defined in the `Job*` POJOs. This requirement could be relaxed to only require the needed fields, as well as to NOT fail on unknown fields.

#### Duplicate Data

While this shouldn't actually cause an issue in the current implementation, further testing would be required.

#### Rate Limits

The service should be designed to handle Rate Limit responses if Spreedly decides to Rate Limit our proxy. A requeing strategy could be enabled so that if a request receives a Rate Limit response, it could be requeued to try again at a later time. This would however probably not allow the `jobclient` to complete in time if a Rate Limit was hit.

### 3. How will the service scale to handle more than 50 concurrent requests?

The service should scale up until the point that the number of 'open' requests exceed the number that can be fit into memory. This is because the implementation of the `JobRepository` uses an in-memory `ConcurrentHashMap`. Currently, this HashMap is only required to hold an upper bound of 50 job ids at a given time. If this number were to increase drastically, so would the amount of memory required by our service. That is to say that the service could be scaled by adding more memory, but only up until a point.

Also, the service will only be able to handle the number of concurrent requests that our server/OS allows. This is because of the thread limit imposed per process. For example, if our server/OS only allowed for 100 threads per process, our service could only handle a MAX of 100 concurrent requests (realistically less because there is a main thread as well as GC thread and other 'housekeeping' required). 'Beefier' servers could be used in order to increase this upper limit.

### 4. What do we need to consider to move this from a single-process service on one node to a multi-process service across several nodes?

If this service were to be scaled to a multi-node cluster, then the in memory `JobRepository` implementation would no longer work. This is because one server (A) could issue the initial `JobCreateRequest` to Spreedly and receive the job id, while the actual callback `JobCompletion` request could be received by an entirely different server (B) with no knowledge of that job. This would cause the thread handling the initial request on server A to never complete, since the callback was received by server B.

To remedy this, a shared data store would be required. I would recommend using a store such as [Redis](http://redis.io/) since the data required to be stored (the open requests) is temporary in nature. Also, Redis is single threaded and incredibly fast, so that response times should not increase that drastically and race conditions at the data store could be avoided. 

A traditional database such as Postgres or MySQL could also be used, however since this data does not need to be stored indefinitely and no indexes or other features of these databases are required, I think this would be overkill for our usage.

Regardless, the software has been written to interact with the `JobRepository` via an interface, so the actual data store used is abstracted away. This should allow for an easy drop-in replacement of an actual database or cache.

